package owl.services.repositories;

import org.springframework.stereotype.Repository;
import owl.domains.DummyClass;
import owl.feddericokz.system.repositories.EntityRepository;

@Repository
public interface DummyClassRepository extends EntityRepository<DummyClass,Long> {
}
