package owl.services.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = {
        "owl.services.repositories"
})
public class OwlRepositoriesAutoConfiguration {
}
