package owl.services.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {
        "owl.services.implementations"
})
public class OwlServicesAutoConfiguration {
}
