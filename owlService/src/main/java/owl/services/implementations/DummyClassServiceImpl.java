package owl.services.implementations;

import org.springframework.stereotype.Service;
import owl.domains.DummyClass;
import owl.domains.DummyClassDto;
import owl.feddericokz.system.domains.EntityDto;
import owl.feddericokz.system.services.support.EntityServiceSupport;
import owl.services.DummyClassService;

@Service
public class DummyClassServiceImpl extends EntityServiceSupport<DummyClass,Long> implements DummyClassService {
    @Override
    protected <S extends EntityDto<DummyClass, Long>> S getDTOInstance() {
        return (S) new DummyClassDto();
    }
}
