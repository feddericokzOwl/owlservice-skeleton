package owl.services.rest.clients;

import org.springframework.cloud.openfeign.FeignClient;
import owl.domains.DummyClass;
import owl.domains.DummyClassDto;
import owl.feddericokz.rest.controllers.EntityRestController;

@FeignClient(value = "dummyClassRestClient", url = "${feign.client.owl.serverUrl}/owl/dummyClass")
public interface DummyClassRestClient extends EntityRestController<DummyClassDto, DummyClass, Long> {
}
