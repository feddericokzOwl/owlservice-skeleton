package owl.services.rest.clients.configuration;

import feign.okhttp.OkHttpClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages = {
        "owl.services.rest.clients"
})
public class OwlRestClientsAutoConfiguration {

    /*
     * Beans
     */

    @Bean
    public OkHttpClient okHttpClient() {
        return new OkHttpClient();
    }

}
