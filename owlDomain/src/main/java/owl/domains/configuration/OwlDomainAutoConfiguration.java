package owl.domains.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EntityScan(basePackages = "owl.domains")
public class OwlDomainAutoConfiguration {
}
