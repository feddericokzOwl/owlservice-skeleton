package owl.domains;

import owl.feddericokz.system.domains.support.ServiceReadySupport;

import javax.persistence.Entity;

@Entity
public class DummyClass extends ServiceReadySupport<Long> {

    /*
     * Methods
     */

    @Override
    public boolean autoValidate() {
        return true;
    }

}
