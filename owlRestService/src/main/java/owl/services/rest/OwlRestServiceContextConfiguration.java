package owl.services.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OwlRestServiceContextConfiguration {

    public static void main(String args[]){
        SpringApplication.run(OwlRestServiceContextConfiguration.class, args);
    }

}
