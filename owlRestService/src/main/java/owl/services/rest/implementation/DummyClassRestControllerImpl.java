package owl.services.rest.implementation;

import org.springframework.web.bind.annotation.RestController;
import owl.domains.DummyClass;
import owl.domains.DummyClassDto;
import owl.feddericokz.rest.controllers.support.EntityRestControllerSupport;
import owl.services.rest.DummyClassRestController;

@RestController
public class DummyClassRestControllerImpl extends EntityRestControllerSupport<DummyClassDto, DummyClass, Long> implements DummyClassRestController {
}
