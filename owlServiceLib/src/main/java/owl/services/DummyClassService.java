package owl.services;

import owl.domains.DummyClass;
import owl.feddericokz.system.services.EntityService;

public interface DummyClassService extends EntityService<DummyClass,Long> {
}
