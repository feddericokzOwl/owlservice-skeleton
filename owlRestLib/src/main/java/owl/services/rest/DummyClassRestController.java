package owl.services.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import owl.domains.DummyClass;
import owl.domains.DummyClassDto;
import owl.feddericokz.rest.controllers.EntityRestController;

@RequestMapping("/owl/dummyClass")
public interface DummyClassRestController extends EntityRestController<DummyClassDto, DummyClass, Long> {
}
